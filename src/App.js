import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import ClientAdmin from './component/ClientAdmin';
import ClientUser from './component/ClientUser';
import RealmAdmin from './component/RealmAdmin';
import SuperAdmin from './component/SuperAdmin';
import Unprotected from './component/Unprotected';
import Protected from './component/Protected';
import NoPermission from './component/NoPermission';

function App(props) {
  return (
    <Router>
    <div className="main-container">
      <nav>
        <img alt="mozark logo" src="https://telecomdrive.com/wp-content/uploads/2021/01/mozark-logo.png" width="400"/>
        <h1>App-1</h1>
        <p>Note : You will only see navigation menu based on your user privileges assigned by keycloak</p>
        <button onClick={()=>props.keycloak.logout()}>LOGOUT</button>
        <ul>
          <li>
            <Link to="/unprotected">Unprotected</Link>
          </li>
          <li>
            {props.keycloak.hasResourceRole('client1-admin') && !!props.keycloak.token ? <Link to="/client1-admin">client1-admin</Link>:  <span> /client1-admin [no access] </span>}
          </li>
          <li>
            {props.keycloak.hasResourceRole('client1-user') && !!props.keycloak.token ? <Link to="/client1-user">Client1-User</Link>:<span> /client1-user [no access] </span>}
          </li>
          <li>
            {props.keycloak.hasRealmRole('realm-client1') && !!props.keycloak.token ? <Link to="/realm-client">Realm-client1</Link>:<span> /realm-client [no access] </span>}
          </li>
          <li>
            {props.keycloak.hasRealmRole('super-admin') && !!props.keycloak.token ? <Link to="/super-admin">Super-Admin</Link>:<span> /super-admin [no access] </span>}
          </li>
          <li>
            {props.keycloak.hasResourceRole('client1-admin') && props.keycloak.hasRealmRole('realm-client1') && !!props.keycloak.token ? <Link to="/protected">Protected</Link>:<span> /protected [no access] </span>}
          </li>
        </ul>
      </nav>
      <h1>kEYCLOAK DEMO : SEE COMPONENT CHANGE BELOW</h1>

      {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
      <Switch>
        <Route path="/unprotected">
           <Unprotected/>
        </Route>
        <Route path="/client1-admin">
          {props.keycloak.hasResourceRole('client1-admin') && !!props.keycloak.token ?  <ClientAdmin/> : <NoPermission/> }
        </Route>
        <Route path="/client1-user">
          {props.keycloak.hasResourceRole('client1-user') && !!props.keycloak.token ?<ClientUser/>: <NoPermission/> }
        </Route>
        <Route path="/realm-client">
           {props.keycloak.hasRealmRole('realm-client1') && !!props.keycloak.token ?<RealmAdmin/>: <NoPermission/> }
        </Route>
        <Route path="/super-admin">
          {props.keycloak.hasRealmRole('super-admin') && !!props.keycloak.token ?<SuperAdmin/>: <NoPermission/> }
        </Route>
        <Route path="/protected">
          {props.keycloak.hasResourceRole('client1-admin') && props.keycloak.hasRealmRole('realm-client1') && !!props.keycloak.token ? <Protected/>: <NoPermission/> }
        </Route>
      </Switch>
    </div>
  </Router>
  );
}

export default App;
